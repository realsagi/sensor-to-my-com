package comport;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class main extends javax.swing.JFrame implements ActionListener{
    
    Timer t = new Timer(1,this);
    private static String stageconnect="";
    private static String s1="",s2="",s3="";
    
    public main() {
        t.start();
        initComponents();
        printliscom();
        setIcon();   
        p1.setBackground(Color.GREEN);
        p2.setBackground(Color.GREEN);
        p3.setBackground(Color.GREEN);
    }
    public void showerror(Exception e){
        JOptionPane.showMessageDialog(this,e,"Error",JOptionPane.ERROR_MESSAGE);
    }
    public void showmessage(String message){
        JOptionPane.showMessageDialog(this,message,"Message",JOptionPane.PLAIN_MESSAGE);
    }
    public void setstatepro(String message){
        main.stageconnect = message;
    }
    public void setp1(String bed1){
        main.s1 = bed1;
    }
    public void setp2(String bed2){
        main.s2 = bed2;
    }
    public void setp3(String bed3){
        main.s3 = bed3;
    }
    @Override
    public void actionPerformed(ActionEvent e){
        if(s1.equals("S11")){
            p1.setBackground(Color.RED);
        }
        if(s1.equals("S10")){
            p1.setBackground(Color.GREEN);
        }
        if(s2.equals("S21")){
            p2.setBackground(Color.RED);
        }
        if(s2.equals("S20")){
            p2.setBackground(Color.GREEN);
        }
        if(s3.equals("S31")){
            p3.setBackground(Color.RED);
        }
        if(s3.equals("S30")){
            p3.setBackground(Color.GREEN);
        }
        else{
        
        }
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        selectcom = new javax.swing.JComboBox();
        comlabel = new javax.swing.JLabel();
        connect = new javax.swing.JButton();
        close = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        statepro = new javax.swing.JLabel();
        p2 = new javax.swing.JPanel();
        p1 = new javax.swing.JPanel();
        bed1 = new javax.swing.JLabel();
        bed2 = new javax.swing.JLabel();
        p3 = new javax.swing.JPanel();
        bed3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Baby Soft V1.0");
        setBounds(new java.awt.Rectangle(300, 250, 0, 0));
        setMaximizedBounds(new java.awt.Rectangle(300, 250, 0, 0));
        setModalExclusionType(java.awt.Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        setName("desktop"); // NOI18N
        setType(java.awt.Window.Type.POPUP);

        selectcom.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        selectcom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectcomActionPerformed(evt);
            }
        });

        comlabel.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        comlabel.setText("COM PORT");

        connect.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        connect.setText("CONNECT");
        connect.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        connect.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                connectMouseClicked(evt);
            }
        });

        close.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        close.setText("CLOSE");
        close.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        close.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                closeMouseClicked(evt);
            }
        });

        jLabel2.setText("สถานะการเชิ่อมต่อ");

        statepro.setForeground(new java.awt.Color(255, 51, 51));
        statepro.setText("OFFLINE");

        p2.setBackground(new java.awt.Color(255, 204, 0));
        p2.setForeground(new java.awt.Color(0, 255, 0));
        p2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        p2.setFocusCycleRoot(true);
        p2.setFocusTraversalPolicyProvider(true);

        javax.swing.GroupLayout p2Layout = new javax.swing.GroupLayout(p2);
        p2.setLayout(p2Layout);
        p2Layout.setHorizontalGroup(
            p2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 80, Short.MAX_VALUE)
        );
        p2Layout.setVerticalGroup(
            p2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 70, Short.MAX_VALUE)
        );

        p1.setBackground(new java.awt.Color(255, 204, 0));
        p1.setForeground(new java.awt.Color(0, 255, 0));
        p1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        javax.swing.GroupLayout p1Layout = new javax.swing.GroupLayout(p1);
        p1.setLayout(p1Layout);
        p1Layout.setHorizontalGroup(
            p1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 80, Short.MAX_VALUE)
        );
        p1Layout.setVerticalGroup(
            p1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 70, Short.MAX_VALUE)
        );

        bed1.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        bed1.setText("BED 1");

        bed2.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        bed2.setText("BED 2");

        p3.setBackground(new java.awt.Color(255, 204, 0));
        p3.setForeground(new java.awt.Color(0, 255, 0));
        p3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        javax.swing.GroupLayout p3Layout = new javax.swing.GroupLayout(p3);
        p3.setLayout(p3Layout);
        p3Layout.setHorizontalGroup(
            p3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 80, Short.MAX_VALUE)
        );
        p3Layout.setVerticalGroup(
            p3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 70, Short.MAX_VALUE)
        );

        bed3.setFont(new java.awt.Font("JasmineUPC", 1, 24)); // NOI18N
        bed3.setText("BED 3");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(160, 160, 160)
                    .addComponent(jLabel2)
                    .addGap(18, 18, 18)
                    .addComponent(statepro, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createSequentialGroup()
                    .addGap(242, 242, 242)
                    .addComponent(p3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createSequentialGroup()
                    .addGap(29, 29, 29)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(bed3)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(connect)
                                .addGap(18, 18, 18)
                                .addComponent(close)
                                .addGap(25, 25, 25))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(comlabel)
                                .addGap(18, 18, 18)
                                .addComponent(selectcom, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))))))
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(p1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(bed1)))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(p2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(bed2)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(selectcom, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comlabel))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(connect)
                    .addComponent(close))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bed2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(bed1, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bed3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(p3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(p2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(p1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(statepro, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        p2.getAccessibleContext().setAccessibleParent(p2);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void connectMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_connectMouseClicked
           
        SerialTest main = new SerialTest();
        main.initialize();
            Thread t=new Thread() {
                @Override
                public void run() {
                    //the following line will keep this app alive for 1000 seconds,
                    //waiting for events to occur and responding to them (printing incoming messages to console).
                    try {
                        Thread.sleep(1000);
                        statepro.setText(stageconnect);
                        selectcom.disable();
                    } 
                    catch (InterruptedException ie) {
                        showerror(ie);
                    }
                }
            };
            t.start();            
    }//GEN-LAST:event_connectMouseClicked

    private void closeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_closeMouseClicked
        System.exit(0);
    }//GEN-LAST:event_closeMouseClicked

    private void selectcomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectcomActionPerformed
        SerialTest main = new SerialTest();
        main.setportname(selectcom.getSelectedItem().toString());
    }//GEN-LAST:event_selectcomActionPerformed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new main().setVisible(true);
            }
        });
    }    
    public final void printliscom(){
        for(int i=0;i<15;i++){
            String comPort = "COM"+i;
            try{
                CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier(comPort);
                selectcom.addItem(portId.getName());
            }
            catch(NoSuchPortException e){
                
            }
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bed1;
    private javax.swing.JLabel bed2;
    private javax.swing.JLabel bed3;
    private javax.swing.JButton close;
    private javax.swing.JLabel comlabel;
    private javax.swing.JButton connect;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel p1;
    private javax.swing.JPanel p2;
    private javax.swing.JPanel p3;
    private javax.swing.JComboBox selectcom;
    private javax.swing.JLabel statepro;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
         setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/img/baby.jpg")));
    }
}

